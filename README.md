# a-sports
# a-sports

## Descriere
Proiectul a-sports este o aplicație Laravel ce reprezintă un magazin online de articole sportive.

## Repository Git
Adresa repository-ului pe GitLab: https://gitlab.upt.ro/andrei.rotariu/a-sports.git .

## Cod Sursă
Întregul cod sursă al proiectului este disponibil în repository-ul menționat mai sus. Fișierele binare compilate nu sunt incluse în repository.

## Pașii de Instalare

1. Asigurați-vă că aveți instalat PHP (versiune >=8.0.), Composer și Node.js pe sistemul dumneavoastră.

2. Cloneazăți repository-ul folosind următoarea comandă:
git clone https://gitlab.upt.ro/andrei.rotariu/a-sports.git


3. Navigați în directorul proiectului:
cd a-sports

4. Rulați următoarele comenzi pentru a instala dependențele PHP și JavaScript:
composer install
npm install

5. Copiați fișierul `.env.example` și redenumiți-l în `.env`. Apoi, setați valorile corespunzătoare pentru variabilele de mediu în fișierul `.env`.
Credențiale actuale bază de date:
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=automatedmanagement
DB_USERNAME=root
DB_PASSWORD=

## Pașii de Compilare

6. Generați cheia de criptare a aplicației folosind comanda:
php artisan key:generate

7. În cazul utilizării xampp mergeti la adresa http://localhost/phpmyadmin/, creati o bază de date goală cu numele "asports" și importați baza de date din repository "asports"

8. Rulați comanda pentru a compila și combina aseturile JavaScript și CSS:
npm run dev

## Pașii de Lansare 
 Rulați serverul de dezvoltare local cu următoarea comandă:
 
1. Porniți xampp si acesați serverele de MySql si Apache.

2. Apoi din VS Code, intrați în terminal și apoi scrie-ți comanda : php artisan serve


## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
